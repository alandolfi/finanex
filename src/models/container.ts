interface Events {
  event_id: string;
  event: string;
  location: string;
  actual_date: string;
  planned_date: string;
  carrier_event: string;
  original_planned_date: Date;
  actual_datetime: string;
  planned_datetime: string;
  original_planned_datetime: string;
  discharge_or_loading_date: string;
  mode: string;
  remarks: string;
  delayed: boolean;
  display_event: string;
  connected_port: string;
}

interface Container {
  tracking_id: string;
  container_number: string;
  carrier_name: string;
  carrier_code: string;
  consignee: string;
  mbl_or_awb_number: string;
  tracking_number: string;
  pol_name: string;
  pod_name: string;
  reference_no: "";
  status: "Active";
  updated_at: string;
  container_type: string;
  container_size: string;
  events: Events[];
  ais_data: string;
  tracking_link: string;
  archived: boolean;
  other_data: {};
  shipment_remark: string;
}

interface ResponseDataBL {
  updated_trackings: Container[];
}
