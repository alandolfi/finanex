import { Box, Container } from "@mui/material";

import ListContainer from "@/component/ListContainers";

export default function Home() {
  return (
    <Container>
      <Box>
        <ListContainer />
      </Box>
    </Container>
  );
}
