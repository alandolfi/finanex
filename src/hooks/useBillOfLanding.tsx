import data from "./finanex-data.json";

const useBillOfLanding = () => {
  return data.updated_trackings as unknown as Container[];
};

export default useBillOfLanding;
