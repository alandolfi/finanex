"use client";
import { Box, Divider, Typography } from "@mui/material";

const Footer = () => {
  return (
    <>
      <Divider />
      <Box sx={{ padding: "1rem" }}>
        <footer>
          <Typography variant="caption">Alan Landolfi</Typography>
        </footer>
      </Box>
    </>
  );
};

export default Footer;
