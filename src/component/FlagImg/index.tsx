interface FlagImgProps {
  countryCode: string;
}

const FlagImg = ({ countryCode }: FlagImgProps) => {
  return (
    // eslint-disable-next-line @next/next/no-img-element
    <img
      loading="lazy"
      width="20"
      src={`https://flagcdn.com/h20/${countryCode.toLowerCase()}.png`}
      alt=""
    />
  );
};

export default FlagImg;
