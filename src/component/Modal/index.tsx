import {
  Box,
  Modal,
  Step,
  StepLabel,
  Stepper,
  Typography,
} from "@mui/material";
import { useMemo } from "react";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const ModalRoute = ({
  container,
  open,
  handleClose,
}: {
  container: Container | null | undefined;
  open: boolean;
  handleClose: () => void;
}) => {
  const activeStep = useMemo(() => {
    return container?.events.findIndex((e) => !e.actual_date);
  }, [container]);

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={{ ...style, background: "white" }}>
        <Stepper
          activeStep={activeStep}
          orientation="vertical"
          alternativeLabel
        >
          {container?.events.map((event) => (
            <Step key={event.event_id}>
              <StepLabel
                sx={{
                  flexDirection: "row !important",
                  gap: "1rem",
                }}
              >
                <Typography>{event.location}</Typography>
                <Typography variant="caption">{event.actual_date}</Typography>
              </StepLabel>
            </Step>
          ))}
        </Stepper>
      </Box>
    </Modal>
  );
};

export default ModalRoute;
