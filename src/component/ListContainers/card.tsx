import { ArrowRightAlt, LocationOn } from "@mui/icons-material";
import {
  Card,
  CardContent,
  Typography,
  Stack,
  Chip,
  Stepper,
  Step,
  Tooltip,
  StepLabel,
  CardActions,
  Button,
} from "@mui/material";
import Link from "next/link";
import { useMemo } from "react";
import LabelHeaderFlag from "./flag";

type CardContainerProps = {
  container: Container;
  onRoute: (container: Container) => void;
};

const CardContainer = ({ container, onRoute }: CardContainerProps) => {
  const activeStep = useMemo(() => {
    return container.events.findIndex((e) => !e.actual_date);
  }, [container]);

  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardContent>
        <Typography variant="body1">{container.carrier_name}</Typography>

        <Stack
          flexDirection="row"
          justifyContent="space-between"
          sx={{ marginTop: "5px" }}
        >
          <LabelHeaderFlag labelCountry={container.pol_name} />
          <ArrowRightAlt />
          <LabelHeaderFlag labelCountry={container.pod_name} />
        </Stack>

        <Stack
          flexDirection="row"
          alignItems="center"
          justifyContent="flex-start"
          sx={{ mt: 1, gap: "1rem" }}
        >
          <Typography variant="body2">
            {container.carrier_code} - {container.tracking_number}
          </Typography>
          <Chip color="success" label="Active" sx={{ height: "20px" }} />
        </Stack>
      </CardContent>
      <CardActions>
        <Button size="small" onClick={() => onRoute(container)}>
          Ver Ruta
        </Button>
        <Button
          onClick={() => (window.location.href = container.tracking_link)}
        >
          Live
        </Button>
      </CardActions>
    </Card>
  );
};

export default CardContainer;
