import {
  Box,
  createStyles,
  Input,
  Link,
  makeStyles,
  TextField,
  Theme,
} from "@mui/material";
import { Stack, style } from "@mui/system";

const styles = {
  link: {
    textDecoration: "none",
    textTransform: "uppercase",
    color: "#4f5052",
    cursor: "pointer",
    fontSize: "14px",
    "&:hover": {
      textDecoration: "underline",
    },
  },
  input: {
    width: "34rem",
    "& > div:nth-of-type(1)": {
      borderRadius: "30px",
      background: "white",
    },
  },
};

const Filters = () => {
  return (
    <Stack
      sx={{ my: 4 }}
      flexDirection="row"
      justifyContent="space-between"
      alignItems="center"
    >
      <TextField
        sx={styles.input}
        name="filter"
        id="outlined-basic"
        label=""
        variant="outlined"
        autoComplete="off"
        placeholder="TRACKING NUMBER / NAME"
      />

      <Stack flexDirection="row" sx={{ gap: "10px" }}>
        <Link sx={styles.link}>Active</Link>
        <Link sx={styles.link}>En Proceso</Link>
        <Link sx={styles.link}>Finalizado</Link>
      </Stack>
    </Stack>
  );
};

export default Filters;
