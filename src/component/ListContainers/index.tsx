"use client";
import useBillOfLanding from "@/hooks/useBillOfLanding";
import { Box, Stack } from "@mui/material";
import CardContainer from "./card";
import React, { useCallback, useState } from "react";
import Filters from "./filter";
import ModalRoute from "../Modal";

const ListContainer = () => {
  const [currentContainer, setCurrentContainer] = useState<Container | null>();
  const containers = useBillOfLanding();
  const onRouteModal = (container: Container) => {
    setCurrentContainer(container);
  };

  const onClose = () => {
    setCurrentContainer(null);
  };

  return (
    <Box sx={{ mt: 4 }}>
      {/* <Filters /> */}

      <Stack flexDirection="row" sx={{ gap: "1rem" }}>
        {containers.map((c) => (
          <CardContainer
            key={c.tracking_id}
            container={c}
            onRoute={onRouteModal}
          />
        ))}
      </Stack>
      <ModalRoute
        open={!!currentContainer}
        container={currentContainer}
        handleClose={onClose}
      />
    </Box>
  );
};

export default ListContainer;
