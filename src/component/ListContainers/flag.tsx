import { Stack, Typography } from "@mui/material";
import FlagImg from "../FlagImg";

const LabelHeaderFlag = ({ labelCountry }: { labelCountry: string }) => {
  const [province, country, code] = labelCountry.split(",");
  if (code) {
    return (
      <Stack
        flexDirection="row"
        justifyContent="center"
        alignItems="center"
        sx={{ gap: "5px" }}
      >
        <FlagImg countryCode={code.slice(0, 3).trim()} />
        <Typography component="span" sx={{ fontSize: "12px" }}>
          {province},{" "}
        </Typography>
        <Typography component="span" sx={{ fontSize: "12px" }}>
          {country}
        </Typography>
      </Stack>
    );
  }
  return (
    <Typography component="span" sx={{ fontSize: "12px" }}>
      -
    </Typography>
  );
};

export default LabelHeaderFlag;
