import { Box, Divider } from "@mui/material";
import Image from "next/image";

const Header = () => {
  return (
    <>
      <Box sx={{ height: "72px", padding: "1rem" }}>
        <Image src={"/logo.webp"} alt="logo" width="175" height="40" />
      </Box>
      <Divider />
    </>
  );
};

export default Header;
